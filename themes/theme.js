export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#006C7B',
      accent: '#ffb445',
      secondary: '#FF802C',
      success: '#01cda8',
      info: '#707070',
      warning: '#FF641E',
      error: '#b50f2c'
    },
    dark: {
      primary: '#006C7B',
      accent: '#ffb445',
      secondary: '#FF802C',
      success: '#01cda8',
      info: '#707070',
      warning: '#FF641E',
      error: '#b50f2c'
    }
  }
}